import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatTableModule} from '@angular/material/table';
import { CrudTableComponent } from './crud-table/crud-table.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import {MatInputModule} from '@angular/material/input'; 
import { RouterModule } from '@angular/router';
import {CreateNewComponent} from './create-new/create-new.component';
import { EditComponent } from './edit/edit.component';
import { DetailsComponent } from './details/details.component';
import { DeleteComponent } from './delete/delete.component';
@NgModule({
  declarations: [
    AppComponent,
    CrudTableComponent,
    CreateNewComponent,
    EditComponent,
    DetailsComponent,
    DeleteComponent
  ],
  imports: [
    BrowserModule,
    MatTableModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatPaginatorModule,
    MatSortModule,
    MatInputModule,
    RouterModule.forRoot([
      {path: 'table', component: CrudTableComponent},
      {path:'create', component: CreateNewComponent},
      {path: 'edit/:id', component: EditComponent},
      {path: 'details/id', component: DetailsComponent},
      {path: 'delete/:id', component: DeleteComponent}
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
