import { Component, OnInit } from '@angular/core';


import {MatInputModule} from '@angular/material/input'; 

@Component({
  selector: 'app-create-new',
  templateUrl: './create-new.component.html',
  styleUrls: ['./create-new.component.sass']
})
export class CreateNewComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
